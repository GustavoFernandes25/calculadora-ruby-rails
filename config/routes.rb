Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/calcular/:number1/:number2', to: 'calcular#home', as: 'home_now'

  get '/adicionar/:number1/:number2', to: 'adicionar#adicionar', as: 'add_now'

  get '/dividir/:number1/:number2', to: 'dividir#dividir', as: 'div_now'

  get '/subtrair/:number1/:number2', to: 'subtrair#subtrair', as: 'sub_now'

  get '/multiplicar/:number1/:number2', to: 'multiplicar#multiplicar', as: 'mult_now'


end

